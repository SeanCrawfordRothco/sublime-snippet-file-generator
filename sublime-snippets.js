var sublimeSnippets = exports = module.exports = {};

sublimeSnippets.parseVar = function(snippet) {
	return snippet.replace(snippet.match(/\$\{.*\:/g),'').replace('}','');
}

sublimeSnippets.parseVars = function(html) {
	var atts = html.match(/\$\{.*?\}/g)
	var finalAtts = {
			labels:[],
			strs:[]
		};

    if(atts !== null){
    	for(var q=0; q<atts.length; q++){
    		if(finalAtts.strs.indexOf(atts[q])==-1){
    			finalAtts.labels.push(sublimeSnippets.parseVar(atts[q]));
    			finalAtts.strs.push(atts[q]);
    		}
    	}
    }

	return finalAtts;
}

sublimeSnippets.parse = function(fileContents,vars){
	String.prototype.stripSlashes = function(){
	    return this.replace(/\\(.)/mg, "$1");
	}

	String.prototype.replaceAll = function(find, replace) {
	  return this.replace(new RegExp(find, 'g'), replace);
	}

	var template = fileContents.split('<![CDATA[')[1].split(']]>')[0].stripSlashes();
	var atts = sublimeSnippets.parseVars(template);

	for(var i in atts.strs){
		template = template.split(atts.strs[i]).join(vars[atts.labels[i]]);
	}

	return template;
};



