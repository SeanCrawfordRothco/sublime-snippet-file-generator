exports = module.exports = {
	component: require('./blueprints/component'),
	componentExt: require('./blueprints/component-ext'),
	componentAtt: require('./blueprints/component-att'),
	block: require('./blueprints/block'),
	blockAtt: require('./blueprints/block-att'),
	route: require('./blueprints/route'),
	routeFlux: require('./blueprints/route-flux'),
	service: require('./blueprints/service')
};