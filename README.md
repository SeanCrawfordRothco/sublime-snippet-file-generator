# Sublime Snippet File Generator *v1.0*

This package allows an application to generate files using Sublime Text snippets as file templates.
Blueprints are used to define a list of one or more files to be generated.

This package uses snippets from [Sean's Sublime Text Snippets][sean snippets] repository.

###
To generate files from a blueprint use the following:

	$ var SublimeGenerator = require('sublime-snippet-file-generator');
	$ SublimeGenerator.generate("component","MyNewComponent");

###Blueprints
The following is a list of the blueprints currently available for use:

- route
- block
- component
- routeExt
- routeFlux

###Upcoming blueprints
- service

[sean snippets]:https://bitbucket.org/SeanCrawford/sublime-text-snippets
