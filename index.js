var fs = require('fs');
var sublimeSnippets = require('./sublime-snippets');

var sublimeSnippetFileGenerator = exports = module.exports = {};

sublimeSnippetFileGenerator.expandDataVariations = function(data){
    var expandedData = {};
    for(var i in data){
        expandedData[i] = data[i];
        expandedData[i + "--lowercase"] = data[i].toLowerCase();
        expandedData[i + "--lowercase-first"] = data[i].charAt(0).toLowerCase() + data[i].slice(1);
        expandedData[i + "--lowercase-dashed"] = data[i].replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
        expandedData[i + "--uppercase"] = data[i].toUpperCase();
    }
    return expandedData;
};

sublimeSnippetFileGenerator.generate = function(type, name){

    var blueprint;

    if(typeof sublimeSnippetFileGenerator.blueprints[type] !== 'undefined'){
        blueprint = sublimeSnippetFileGenerator.blueprints[type](name);
    } else {
        console.error('Blueprint:'+type+ ' does not exist.');
        return;
    }

    for(var i in blueprint.folders){
        try {
            fs.mkdirSync(blueprint.folders[i]);
        } catch(e){

        }
    }

    for(var i in blueprint.files){
        if(typeof blueprint.files[i].snippet !== 'undefined'){
            var snippet = fs.readFileSync(blueprint.files[i].snippet, 'utf8');
            var data = sublimeSnippetFileGenerator.expandDataVariations(blueprint.files[i].data);
            var parsedSnippet = sublimeSnippets.parse(snippet,data);
            fs.writeFileSync(blueprint.files[i].filename,parsedSnippet);
        } else {
            fs.writeFileSync(blueprint.files[i].filename,'');
        }
    }
};

sublimeSnippetFileGenerator.blueprints = require('./blueprints');