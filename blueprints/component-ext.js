exports = module.exports = function(name){
	var location = "../Components/";
	return {
		dest: location,
		folders:[
			location+name
		],
		files:[
			{
				snippet:'node_modules/sublime-text-snippets/js/typescript/component/component-controller.sublime-snippet',
				filename: location+name+'/'+name+'Controller.ts',
				data:{
					component: name
				}
			},
			{
				snippet:'node_modules/sublime-text-snippets/js/typescript/component/component-ext-directive.sublime-snippet',
				filename: location+name+'/'+name+'.ts',
				data:{
					component: name
				}
			},
			{
				filename: location+name+'/'+name+'.scss'
			},
			{
				filename: location+name+'/'+name+'.html'
			}
		]
	}
};