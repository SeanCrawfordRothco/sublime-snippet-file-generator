exports = module.exports = function(name){
	var location = "app/pipeline/Blocks/";
	return {
		dest: location,
		folders:[
			location+name
		],
		files:[
			{
				snippet:'node_modules/sublime-text-snippets/js/typescript/block/block-controller-attachable.sublime-snippet',
				filename: location+name+'/'+name+'Controller.ts',
				data:{
					block: name
				}
			},
			{
				filename: location+name+'/'+name+'.scss'
			}
		]
	}
};