exports = module.exports = function(name){
	var location = "app/pipeline/Routes/";
	return {
		dest: location,
		folders:[
			location+name
		],
		files:[
			{
				snippet:'node_modules/sublime-text-snippets/js/typescript/route/flux/route-flux-controller.sublime-snippet',
				filename: location+name+'/'+name+'Controller.ts',
				data:{
					route: name,
					serviceName: name.charAt(0).toLowerCase() + name.slice(1)
				}
			},
			{
				snippet:'node_modules/sublime-text-snippets/js/typescript/route/flux/route-flux-actions.sublime-snippet',
				filename: location+name+'/'+name+'Actions.ts',
				data:{
					route: name,
					serviceName: name.charAt(0).toLowerCase() + name.slice(1)
				}
			},
			{
				snippet:'node_modules/sublime-text-snippets/js/typescript/route/flux/route-flux-store.sublime-snippet',
				filename: location+name+'/'+name+'Store.ts',
				data:{
					route: name,
					serviceName: name.charAt(0).toLowerCase() + name.slice(1)
				}
			},
			{
				snippet:'node_modules/sublime-text-snippets/js/typescript/route/route-template.sublime-snippet',
				filename: location+name+'/'+name+'.html',
				data:{
					route: name
				}
			}
		]
	}
};