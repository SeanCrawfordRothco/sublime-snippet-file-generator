exports = module.exports = function(name){
	var location = "app/pipeline/Components/";
	return {
		dest: location,
		folders:[
			location+name
		],
		files:[
			{
				snippet:'node_modules/sublime-text-snippets/js/typescript/component/component-controller-attachable.sublime-snippet',
				filename: location+name+'/'+name+'Controller.ts',
				data:{
					component: name
				}
			},
			{
				filename: location+name+'/'+name+'.scss'
			}
		]
	}
};