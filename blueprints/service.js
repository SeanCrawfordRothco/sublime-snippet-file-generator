exports = module.exports = function(name){
	var location = "app/pipeline/Services/";
	return {
		dest: location,
		folders:[
			location+name
		],
		files:[
			{
				snippet:'node_modules/sublime-text-snippets/js/typescript/service/service.sublime-snippet',
				filename: location+name+'/'+name+'Service.ts',
				data:{
					service: name
				}
			},
			{
				snippet:'node_modules/sublime-text-snippets/js/typescript/service/service-spec.sublime-snippet',
				filename: location+name+'/'+name+'Service.spec.js',
				data:{
					service: name
				}
			}
		]
	}
};