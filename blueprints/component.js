exports = module.exports = function(name){
	var location = "app/pipeline/Components/";
	return {
		dest: location,
		folders:[
			location+name
		],
		files:[
			{
				snippet:'node_modules/sublime-text-snippets/js/typescript/component/component-controller.sublime-snippet',
				filename: location+name+'/'+name+'Controller.ts',
				data:{
					component: name
				}
			},
			{
				snippet:'node_modules/sublime-text-snippets/js/typescript/component/component-directive.sublime-snippet',
				filename: location+name+'/'+name+'.ts',
				data:{
					component: name
				}
			},
			{
				snippet:'node_modules/sublime-text-snippets/js/typescript/component/component-spec.sublime-snippet',
				filename: location+name+'/'+name+'.spec.js',
				data:{
					component: name
				}
			},
			{
				filename: location+name+'/'+name+'.scss'
			},
			{
				snippet:'node_modules/sublime-text-snippets/js/typescript/emptydiv.sublime-snippet',
				filename: location+name+'/'+name+'.html',
				data:{
					component: name
				}
			}
		]
	}
};