exports = module.exports = function(name){
	var location = "app/pipeline/Routes/";
	return {
		dest: location,
		folders:[
			location+name
		],
		files:[
			{
				snippet:'node_modules/sublime-text-snippets/js/typescript/route/route-controller.sublime-snippet',
				filename: location+name+'/'+name+'Controller.ts',
				data:{
					route: name
				}
			},
			{
				snippet:'node_modules/sublime-text-snippets/js/typescript/route/route-template.sublime-snippet',
				filename: location+name+'/'+name+'.html',
				data:{
					route: name
				}
			}
		]
	}
};