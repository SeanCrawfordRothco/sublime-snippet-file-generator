exports = module.exports = function(name){
	var location = "app/pipeline/Blocks/";
	return {
		dest: location,
		folders:[
			location+name
		],
		files:[
			{
				snippet:'node_modules/sublime-text-snippets/js/typescript/block/block-controller.sublime-snippet',
				filename: location+name+'/'+name+'Controller.ts',
				data:{
					block: name
				}
			},
			{
				snippet:'node_modules/sublime-text-snippets/js/typescript/block/block-directive.sublime-snippet',
				filename: location+name+'/'+name+'.ts',
				data:{
					block: name
				}
			},
			{
				filename: location+name+'/'+name+'.scss'
			},
			{
				snippet:'node_modules/sublime-text-snippets/js/typescript/emptydiv.sublime-snippet',
				filename: location+name+'/'+name+'.html',
				data:{
					block: name
				}
			}
		]
	}
};